
# Simple MLM

A Simple MLM System to show members in a hierarchical tree. show members' level and bonus based on their downline



## Installation

Project Requirements

```
  npm v18.7.0
```

Install Simple MLM with npm

```bash
  npm install
```

Run the app

```bash
  npm start
```

Install PM2 to serve the web app

```bash
  npm install -g pm2
```
    
Serve the web app using pm2

```bash
  pm2 serve web/ --name=simple-mlm-web --port=8080
```

you can access the web app on http://127.0.0.1:8080

## API Reference

API Host : http://127.0.0.1:6600

#### Get Member

```http
  GET /api/members
```


#### Add member

```http
  POST /api/members
```

| Body | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. Member's name  |
| `parent`      | `string` | **Optional**. Parent's id. if empty firstMember must be **true** |
| `firstMember`      | `string` | **Optional**. declares if member will have no parent  |


#### Get Children of member

```http
  GET /api/members/children/:id
```

| Params | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Member's id  |

#### Migrate member

```http
  PUT /api/members/change-parent/:id
```

| Body | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Member's id  |
| `parent`      | `string` | **Optional**. Parent's id. if empty firstMember must be **true** |
| `firstMember`      | `string` | **Optional**. declares if member will have no parent  |


#### Get Member's Bonus

```http
  GET /api/members/bonus/:id?level=number
```

| Params | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Member's id  |

| Query | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `level`      | `string` | **Required**. Downline level  |

#### Get Parents Member avaiable for migration

```http
  GET /api/members/parents-to-migrate/:id
```

| Params | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Member's id  |
