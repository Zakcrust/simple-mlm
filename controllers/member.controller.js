const Member = require('../schemas/member.schema').model;
const { body } = require('express-validator');
var ObjectId = require('mongoose').Types.ObjectId;

exports.getMemberTree = async (req, res) => {
    try {
        const data = await Member.find().lean().populate('children');
        for (let index = 0; index < data.length; index++) {
            var children = await traverseChildren(data[index], [], 1);
            data[index].bonus = countBonus(children, data[index].level);
        }
        return res.status(200).send({
            message: "Member tree has been retrieved",
            data: data
        })
    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error)
        })
    }
}

exports.validate = async (method) => {
    switch (method) {
        case 'add-member':
            return [
                body('name', 'name must not be empty').exists()
            ]
        default:
            return [];
    }
}

exports.addFirstMember = async (req, res) => {
    try {
        const data = await Member.create({
            name: req.body.name,
            level: 1
        })
        return res.status(201).send({
            message: "Member data has been created",
            data: data
        })
    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error)
        })
    }

}

exports.removeChild = async (req, res) => {
    try {
        const child = await Member.findById(req.body.child)
        if (!child) {
            return res.status(404).send({
                message: "Child not found"
            })
        }
        var parent = await Member.findById(req.params.id).populate('children')
        if (parent) {
            console.log(`before removal ${parent.children}`)
            var children = parent.children.filter((value) => value.id !== child.id)
            console.log(`after removal ${children}`)
            parent.children = children
            parent = await parent.save()
            return res.status(200).send({
                message: `Child ${child.name} has been removed from ${parent.name}`
            })
        }
        else {
            return res.status(404).send({
                message: "Parent not found"
            })
        }
    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error)
        })
    }
}

exports.reset = async (req, res) => {
    try {
        await Member.deleteMany()
        return res.status(200).send({
            message: "Reset members collection"
        })
    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error)
        })
    }
}

exports.addMember = async (req, res) => {
    try {
        const parent = await Member.findById(req.body.parent)
        if (!parent) {
            return res.status(404).send({
                message: "Parent not found"
            })
        }
        else {
            req.body.level = parent.level + 1;
        }
        const data = await Member.create({
            name: req.body.name,
            level: req.body.level,
            parent: req.body.parent
        })
        await Member.findByIdAndUpdate(req.body.parent, {
            children: [...parent.children, data.id]
        })
        return res.status(201).send({
            message: "Member data has been created",
            data: data
        })
    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error)
        })
    }
}

exports.getChildren = async (req, res, next) => {
    try {
        const parent = await Member.findById(req.params.id).lean().populate('children')
        if (parent) {
            var children = await traverseChildren(parent, [], 1)
            return res.status(200).send({
                message: `Children of ${parent.name} has been retrieved`,
                data: children
            })
        }
    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error, null, 2)
        })
    }

}

exports.parentsToMigrate = async (req, res) => {
    try {
        const member = await Member.findById(req.params.id).populate('children')
        if (!member) {
            return res.status(404).send({
                message: "Member not found"
            })
        }
        var data = await Member.find();
        var children = await traverseChildren(member, [], 1)
        var parent = data.filter((value) => 
            !children.find((val) => val.id == value.id) &&
            value.id !== member.id
        )
        if(member.parent !== undefined && member.parent !== null)
        {
            parent = parent.filter((value) => value.id !== member.parent.toString())
        }
        return res.status(200).send({
            message: `Possible future parent for member ${member.name} has been retrieved`,
            data: parent
        })
    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error, null, 2)
        })
    }
}

exports.migrateMember = async (req, res) => {
    try {
        var member = await Member.findById(req.params.id).populate('children');
        if (!member) {
            return res.status(404).send({
                message: "Member not found"
            })
        }
        if (req.body.firstMember == 'true') {
            var parent = await Member.findById(member.parent).populate('children')
            if (parent) {
                parent.children = parent.children.filter((value) => value.id !== member.id)
                parent = await parent.save()
            }
            await Member.findByIdAndUpdate(req.params.id, {
                parent: null,
                level: 1
            });
            var updatedMember = await Member.findById(req.params.id).lean().populate('children')
            var children = await traverseChildren(updatedMember, [], 1)
            await levelCheck(children)

            return res.status(200).send({
                message: "Member has been migrated to first level (no parent)",
                data: member
            })
        }
        else {
            var parent = await Member.findById(req.body.parent)
            if (!parent) {
                return res.status(404).send({
                    message: "Parent not found"
                })
            }
            if(member.parent !== undefined && member.parent !== null)
            {
                var prevParent = await Member.findById(member.parent).populate('children');
                prevParent.children = prevParent.children.filter((value) => value.id !== member.id)
                await prevParent.save()
            }

            member.parent = parent.id;
            member.level = parent.level + 1;
            member = await member.save()
            parent.children.push(member._id)
            await parent.save()
            var updatedMember = await Member.findById(req.params.id).lean().populate('children')
            var children = await traverseChildren(updatedMember, [], 1)
            await levelCheck(children)
            return res.status(200).send({
                message: `Member's parent has been changed to ${parent.name}`,
                data: member
            })
        }

    } catch (error) {
        console.log(error)
        return res.status(500).send({
            message: error.message || JSON.stringify(error, null, 2)
        })
    }
}

exports.countMemberBonus = async (req, res) => {
    try {
        const member = await Member.findById(req.params.id).lean().populate('children')
        if (req.query.level !== undefined && req.query.level !== null) {
            if (req.query.level > member.level + 2 || req.query.level <= member.level) {
                return res.status(400).send({
                    message: "Level must be more than member's level. lesser than +2 member's level"
                })
            }
            var children = await traverseChildren(member, [], 1)
            children = children.filter((value) => value.level == req.query.level)
            console.log(children);
            var bonus = countBonus(children, member.level)

            return res.status(200).send({
                message: "Member's bonus has been retrieved",
                data: {
                    member: {
                        id: member.id,
                        name: member.name,
                        level: member.level
                    },
                    downline_level: parseInt(req.query.level),
                    children: children,
                    bonus: bonus
                }
            })
        }
        else {
            return res.status(400).send({
                message: "level must not be Empty"
            })
        }

    } catch (error) {
        return res.status(500).send({
            message: error.message || JSON.stringify(error, null, 2)
        })
    }
}

var countBonus = (children, level) => {
    var bonus = 0;
    for (let index = 0; index < children.length; index++) {
        const element = children[index];
        var bonus_level = element.level - level;
        if (bonus_level == 1) {
            bonus += 1;
        }
        if (bonus_level == 2) {
            bonus += 0.5
        }
    }
    return bonus;
}

var levelCheck = async (members) => {
    for (let index = 0; index < members.length; index++) {
        const element = members[index];
        var parent = await Member.findById(element.parent)
        if (parent) {
            await Member.findByIdAndUpdate(element._id, {
                level: parent.level + 1
            })
        }
    }

}

var traverseChildren = async (parent, members, depth) => {
    var parents = []
    if (depth <= 0) return members;
    if (members.length === 0) {
        if (parent.children.length == 0) {
            return members;
        }
        var children = parent.children

        members = [...parent.children]

        for (let index = 0; index < children.length; index++) {
            var element = await Member.findById(children[index]._id).lean().populate('children')
            if (element.children.length > 0) {
                members = [...members, ...element.children]
                parents = [...parents, ...element.children]
            }
        }
        return traverseChildren(parents, members, depth - 1)
    }
    else {
        if (parent.length == 0) return members
        for (let index = 0; index < parent.length; index++) {
            var element = parent[index]
            element = await Member.findById(element._id).lean().populate('children');
            if (element.children.length > 0) {
                members = [...members, ...element.children]
                parents = [...parents, ...element.children]

            }
        }

        return traverseChildren(parents, members, depth - 1)

    }

}