var express = require('express');
var router = express.Router();
const controller = require('../controllers/member.controller');
/* GET home page. */
router.get('/', [], controller.getMemberTree);

router.post('/', [], (req, res, next) => {
    console.log(req.body)
    if (req.body.firstMember !== undefined) {
        if (req.body.firstMember == 'true') {
            controller.addFirstMember(req, res)
        }
        else {
            controller.addMember(req, res)
        }
    }
    else {
        controller.addMember(req, res)

    }
})

router.put('/change-parent/:id', [], controller.migrateMember)

router.get('/parents-to-migrate/:id', controller.parentsToMigrate);

router.get('/children/:id', [], controller.getChildren)

router.get('/bonus/:id', [], controller.countMemberBonus)

router.put('/child/delete/:id', controller.removeChild)

router.delete('/reset', [], controller.reset);

module.exports = router;
