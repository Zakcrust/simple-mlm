const mongoose = require('mongoose')

const memberSchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true }, // String is shorthand for {type: String}
    level: { type: Number, required: true },
    // bonus: { type: Number, default: 0 },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Member'
    },
    children: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Member'
        }
    ]
}
);


const member = {
    schema: memberSchema,
    model: mongoose.model('Member', memberSchema)
}

module.exports = member;